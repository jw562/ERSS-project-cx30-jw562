<center>Final project</center>
=======================================================
|Group|teammates|
|---|---|
|UPS|cx30, jw562|

## Contents
* [Configuration](#configuration)
* [Support Manual](#support-manual)
* [Additional Files](#additional-files)

<h2 id="configuration">Configuration</h2>
1. Run the world.
2. Run Amazon.
3. Run "sudo docker-compose up".
4. Visit "http://vcm-4013.vm.duke.edu:8000/ups/homepage/", and this is the homepage of our UPS.
<h2 id="support-manual">Support Manual</h2>
<h4> Below is the guidence for our website</h4>
<h5>Register/Login</h5>
Login button is on the upper-left of our web page. Press login and then you can find a link to register page.
![login](https://i.imgur.com/5GDrCep.png)
![register](https://i.imgur.com/HupQ6cC.png)
<h5>Trace your package in trace page.</h5>
![trace](https://i.imgur.com/YQogUzh.png)
<h5>Find all your packages in profile page after logging in.</h5>
![profile](https://i.imgur.com/ZIj9VG3.png)
<h5>Please contact us if you have any questions</h5>
<h2 id="additional-files">Additional Files</h2>
1. [Protocal Between AMZ and UPS](https://gitlab.oit.duke.edu/jw562/ERSS-project-cx30-jw562/blob/master/final%20project%20protocal.pdf)
2. [Project differenciation](https://gitlab.oit.duke.edu/jw562/ERSS-project-cx30-jw562/blob/master/ERSS-Project%20Features%20and%20Discussion.pdf)