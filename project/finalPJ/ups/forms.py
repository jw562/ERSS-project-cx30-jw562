from django import forms
from django.forms import ModelForm,modelformset_factory
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import *


class RegisterForm(UserCreationForm): #UserCreationForm
    username   = forms.CharField(max_length=50)
    first_name = forms.CharField()
    last_name  = forms.CharField()
    password1  = forms.CharField(widget=forms.PasswordInput())
    password2  = forms.CharField(widget=forms.PasswordInput())
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ('username','first_name','last_name','password1', 'password2','email')
        def __init__(self,*args, **kwargs):
            super(RegisterForm, self).__init__(*args, **kwargs)    


class TraceForm(forms.Form):
    package_id = forms.IntegerField(label='package_id')
    
class RedirectForm(forms.Form):
    xdest = forms.IntegerField(label='xdest')
    ydest = forms.IntegerField(label='ydest')
    
class SearchForm(forms.Form):
    description = forms.CharField(max_length=50)

    
