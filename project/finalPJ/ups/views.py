from django.shortcuts import render,render_to_response,get_object_or_404
from django.utils import timezone
from django.shortcuts import redirect
from django import forms
from django.http import HttpResponse,HttpResponseRedirect
from django.template import RequestContext
from .forms import *
from django.contrib.auth.models import User
from .models import *
from django.core.mail import EmailMultiAlternatives
import socket,re
from django.contrib.auth.models import AbstractUser
from django.core.mail import send_mail
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse_lazy
from django.db import transaction
import socket
'''
from guardian.shortcuts import assign_perm, get_perms, remove_perm
from guardian.core import ObjectPermissionChecker
from guardian.decorators import permission_required
'''
# Create your views here.
wrong_email = "The email format is wrong"
AnswerDoesNotExist = "This question has no answer"
have_regist = "You have responded to this event"
wrong_login = "Your username or email or password is wrong"
wrong_user = "The user is not alive"
no_history = "History dose not exist"
wrong_package = "The package does not exist"
wrong_product = "The package is empty"

class UserForm(forms.Form):
    username = forms.CharField(label = 'username',max_length=50)
    email = forms.CharField(label = 'email',max_length=50)
    password = forms.CharField(label = 'password',max_length=50,widget=forms.PasswordInput())
  

def register(request):
    if request.method == 'POST':
        uf = RegisterForm(request.POST)
        if uf.is_valid():
            #get data
            uf.save()
            username = uf.cleaned_data.get('username')
            password = uf.cleaned_data.get('password1')
            first_name = uf.cleaned_data.get('first_name')
            last_name = uf.cleaned_data.get('last_name')
            email = uf.cleaned_data.get('email')
            return render(request,'ups/homepage.html',{'uf':uf})           
    else:
        uf = RegisterForm()
    return render (request,'ups/register.html',{'uf':uf})

def signin(request):
    if request.method == 'POST':
        uf = UserForm(request.POST)
        if uf.is_valid():
            #get username and password
            username = uf.cleaned_data['username']
            password = uf.cleaned_data['password']
            #compare with database
            #user = User.objects.filter(username__exact = username,password__exact = password)
        username = request.POST['username']
        raw_password = request.POST['password']
        user = authenticate(username=username, password=raw_password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('/ups/homepage/')
            else:
                return render(request,'ups/wrong.html',{'wrong_message':wrong_user})
        else:
            return render(request,'ups/wrong.html',{'wrong_message':wrong_login})                    
    else:
        uf = UserForm()
    return render (request,'ups/login.html',{'uf':uf})

@login_required(login_url=reverse_lazy('login')) 
def signout(request):
    logout(request)
    return render(request,'ups/homepage.html')
        
def homepage(request):
    return render(request,'ups/homepage.html')

@login_required(login_url=reverse_lazy('login'))          
def profile(request):
    username = request.user.username
    mypackages = Package.objects.filter(ups_account = username)
    return render(request,'ups/profile.html',{'username':username, 'mypackages':mypackages})

'''
@login_required(login_url=reverse_lazy('login'))    
def search(request):
    if request.method == 'POST':
        uf = SearchForm(request.POST)
        if uf.is_valid():
            description = uf.cleaned_data.get('description')
            mypackages = Package.objects.filter(ups_account = username)
            
            productlist = package.product.all()
            return render(request,'ups/package_detail.html',{'package':package,'productlist':productlist})
    else:
        uf = TraceForm()
    return render(request,'ups/trace.html',{'uf':uf})
'''

def redirection(request,pk):
    #get data
    xdest = rf.cleaned_data.get('xdest')
    ydest = rf.cleaned_data.get('ydest')
    with transaction.atomic():
        package = Package.objects.select_for_update().get(pk = pk)
        package.x_dest = xdest
        package.y_dest = ydest
        package.save()
        #send email to user
        mesg = 'You have redirected your package successfully!'
        send_mail('Redirect',mesg,'wangjunru562@gmail.com',[email],fail_silently=False,)

def trace(request):
    if request.method == 'POST':
        uf = TraceForm(request.POST)
        if uf.is_valid():
            #get data
            package_id = uf.cleaned_data.get('package_id')
            #search by package_id
            try:
                package = Package.objects.get(package_id = package_id)
            except ObjectDoesNotExist:
                return render(request,'ups/wrong.html',{'wrong_message':wrong_package})
            try:
                productlist = package.product.all()
            except ObjectDoesNotExist:
                return render(request,'ups/wrong.html',{'wrong_message':wrong_product})
            try:
                history = package.history
            except ObjectDoesNotExist:
                return render(request,'ups/wrong.html',{'wrong_message':no_history})
            if request.method == 'POST':
                rf = RedirectForm(request.POST)
                if rf.is_valid():
                    redirection(request,pk)
                    return render(request,'ups/package_detail.html',{'package':package,'productlist':productlist,'rf':rf,'history':history})
            else:
                rf = RedirectForm()
            return render(request,'ups/package_detail.html',{'package':package,'productlist':productlist,'rf':rf,'history':history})
    else:
        uf = TraceForm()
    return render(request,'ups/trace.html',{'uf':uf})


@login_required(login_url=reverse_lazy('login'))       
def package_detail(request,pk):    
    package = get_object_or_404(Package,pk=pk)
    productlist = package.product.all()
    username = package.ups_account
    user = get_object_or_404(User,username=username)
    email = user.email
    if request.user == user:
        try:
            history = package.history
        except ObjectDoesNotExist:
            return render(request,'ups/wrong.html',{'wrong_message':no_history})
        if request.method == 'POST':
            rf = RedirectForm(request.POST)
            if rf.is_valid():
                redirection(request,pk)
                return render(request,'ups/package_detail.html',{'package':package,'productlist':productlist,'rf':rf,'history':history})
        else:
            rf = RedirectForm()
        return render(request,'ups/package_detail.html',{'package':package,'productlist':productlist,'rf':rf,'history':history})  
    else:
        return render(request,'ups/login.html') 

def wrong(request):
    return render(request,'ups/wrong.html',{'wrong':wrong}) 
'''
def communicate(request):
    address = ('vcm-4013.vm.duke.edu', 9999)  
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # s = socket.socket()  
    s.bind(address)  
    s.listen(5)  
    ss, addr = s.accept()  
    data = ss.recv(1024)  
    ss.close()  
    s.close()
    return data
'''
def aboutus(request):
    #data = communicate(request)
    return render(request,'ups/aboutus.html')
