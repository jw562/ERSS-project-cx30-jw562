# Generated by Django 2.0.4 on 2018-04-22 20:13

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Package',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('package_id', models.IntegerField(default=0, unique=True)),
                ('order_num', models.IntegerField(default=0, null=True, unique=True)),
                ('ups_account', models.CharField(max_length=20, null=True)),
                ('status', models.IntegerField(choices=[(0, 'package created'), (1, 'package en route to warehouse'), (2, 'waiting for package'), (3, 'package out for delivery'), (4, 'package delivered')], default=0)),
                ('whid', models.IntegerField(default=0)),
                ('x_dest', models.IntegerField(default=0)),
                ('y_dest', models.IntegerField(default=0)),
                ('truckid', models.IntegerField(default=0)),
                ('time', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('AProduct_id', models.IntegerField(default=0)),
                ('description', models.TextField(max_length=1000)),
                ('count', models.IntegerField(default=0)),
                ('product', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='product', to='ups.Package', to_field='package_id')),
            ],
        ),
    ]
