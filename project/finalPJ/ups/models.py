from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.core import mail
from django.core.mail import EmailMultiAlternatives
import datetime

# Create your models here.

class Package(models.Model):
    CREATED = 0
    EN_ROUTE_TO_WH = 1
    WAITING_FOR_PACKAGE = 2
    OUT_FOR_DELIVERY = 3
    DELIVERED = 4
    STATUS_CHOICES = (
        (CREATED, "package created"),
        (EN_ROUTE_TO_WH, "package en route to warehouse"),
        (WAITING_FOR_PACKAGE, "waiting for package"),
        (OUT_FOR_DELIVERY, "package out for delivery"),
        (DELIVERED, "package delivered"),
    )
     
    package_id = models.BigIntegerField(default = 0,null=False,unique=True)    
    order_num = models.BigIntegerField(default = 0,null=True,unique=True) 
    ups_account = models.CharField(max_length = 20,null=True)
    status = models.IntegerField(default = CREATED, choices=STATUS_CHOICES)
    whid = models.IntegerField(default = 0,null=False)
    x_dest = models.IntegerField(default = 0,null=False)
    y_dest = models.IntegerField(default = 0,null=False)
    truckid = models.IntegerField(default = 0,null=False)
    time = models.DateTimeField(blank=True, null=False,default=timezone.now)
  
    def __str__(self):
        return self.package_id
      
        
class Product(models.Model):
    AProduct_id = models.IntegerField(default = 0,null=False)
    product = models.ForeignKey(Package,null = True,on_delete = models.CASCADE,related_name="product",to_field='package_id')
    description = models.TextField(max_length = 1000)
    count = models.IntegerField(default = 0,null=False)
    
    def __str__(self) :
        return self.description        
        
class History(models.Model):
     package = models.OneToOneField(Package,default = 0,null=False,on_delete = models.CASCADE,to_field='package_id')
     created = models.DateTimeField(blank=True, null=True,default=timezone.now)
     enroute = models.DateTimeField(blank=True, null=True,default=timezone.now)
     wfpackage = models.DateTimeField(blank=True, null=True,default=timezone.now)
     endelivery = models.DateTimeField(blank=True, null=True,default=timezone.now)
     delivered = models.DateTimeField(blank=True, null=True,default=timezone.now)
     
     def __str__(self) :
        return self.package_id  