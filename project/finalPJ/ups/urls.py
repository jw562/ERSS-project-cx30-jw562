from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^login/$', views.signin,name='login'),
    url(r'^register/$', views.register,name='register'),
    url(r'^logout/$',views.signout,name='logout'),
    url(r'^homepage/$', views.homepage, name='homepage'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^trace/$', views.trace, name='trace'),
    url(r'^aboutus/$', views.aboutus, name='aboutus'),
    url(r'^error/$', views.wrong, name='wrong'),
    url(r'^package/(?P<pk>[0-9]+)/$', views.package_detail, name='package_detail'),
]
